trigger OpportunityTrigger on Opportunity (before update) {
    
    List<Contact> conList;
    Map<String,String> accToOppIdsMap = new Map<String,String>();
    if(Trigger.isUpdate && Trigger.isBefore && OppotunityTrigger_Handler.isFirstRun == true){
        System.debug('Inside Before Update');
        for(Opportunity opp : Trigger.new){
            if(opp.StageName != Trigger.oldMap.get(opp.Id).StageName){
                if(opp.StageName == 'Needs Analysis' && (opp.AccountId != null || opp.AccountId != '')){
                    System.debug('Inside If');
                    accToOppIdsMap.put(opp.AccountId,opp.Id);
                }
                else if(opp.StageName == 'Qualification' && opp.Has_Check_Inventory_task_completed__c == true){
                    System.debug('Inside else if');
                    opp.Has_Check_Inventory_task_completed__c = false;
                }
            }
        }
        if(!accToOppIdsMap.isEmpty()){
            conList = [SELECT Id,AccountId,Email FROM Contact WHERE AccountId IN : accToOppIdsMap.keySet() AND Email != null];
            if(!conList.isEmpty()){
                for(String accId : accToOppIdsMap.keySet()){
                    OppotunityTrigger_Handler.sendEmail(conList,accToOppIdsMap.get(accId));
                }
            }
        }
    }
    
}