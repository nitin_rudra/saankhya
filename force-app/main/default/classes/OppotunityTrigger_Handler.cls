public class OppotunityTrigger_Handler {
    
    public static Boolean isFirstRun = true;
    
    
    /*public static void beforeUpdateEvent(List<Opportunity> newOppList,Map<Id,Opportunity> oldOppMap){
       List<Contact> conList = new List<Contact>();
    	Map<String,String> accToOppIdsMap = new Map<String,String>();
        if(!newOppList.isEmpty()){
            for(Opportunity opp : newOppList){
                if(opp.StageName != oldOppMap.get(opp.Id).StageName && opp.StageName == 'Needs Analysis' && (opp.AccountId != null || opp.AccountId != '')){
                	System.debug('Inside If');
                    accToOppIdsMap.put(opp.AccountId,opp.Id);  
                }
                else if(opp.StageName == 'Qualification' && opp.Has_Check_Inventory_task_completed__c == true){
                    System.debug('Inside else if');
                    opp.Has_Check_Inventory_task_completed__c = false;
                }
            }
        }
        if(!accToOppIdsMap.isEmpty()){
            System.debug('Inside iffff2');
            conList = [SELECT Id,AccountId,Email FROM Contact WHERE AccountId IN : accToOppIdsMap.keySet() AND Email != null];
            System.debug('conListTrigger::: ' + conList);
            if(!conList.isEmpty()){
                for(String accId : accToOppIdsMap.keySet()){
                    System.debug('Inside for');
                    sendEmail(conList,accToOppIdsMap.get(accId));
                }
            }
            
        }
        isFirstRun = false;
        
    }*/
    
    public static void sendEmail(List<Contact> conList,String oppId){
        System.debug('Inside SendEmail Method');
        System.debug('conList::::::: ' + conList);
        System.debug('oppId:: ' + oppId);
        List<String> toAddresses = new List<String>();
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();   
        EmailTemplate emailTemplate = [Select Id from EmailTemplate where Name = 'Oppotunity with Products'];
        System.debug('emailTemplate.Id::: ' + emailTemplate.Id);
        Messaging.SingleEmailMessage mail;
        System.debug('Before If');
        System.debug('conList::: '+conList+'  oppId::: ' +oppId);
        if(! conList.isEmpty() && oppId != null){
            System.debug('Inside if');
            for(Contact con : conList){
                System.debug('Inside for');
                toAddresses.add(con.Email);
                mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(emailTemplate.Id);
                System.debug('toAddresses::: ' + toAddresses);
                if(! toAddresses.isEmpty())
                	mail.setToAddresses(toAddresses);
                mail.setTargetObjectId(con.Id);
                mail.setWhatId(oppId);
                mail.setTreatTargetObjectAsRecipient(false);
                mail.setSaveAsActivity(false);
                emails.add(mail);
                if(! emails.isEmpty())
                	Messaging.sendEmail(emails);
                System.debug('After Sending Email');
            }
        }
        isFirstRun = false;
    }
    
    
}